import type { Metadata } from "next";

// Fonts
import { Inter } from "next/font/google";

// STYLES
import "./globals.css";

// CONSTANTS
const inter = Inter({ subsets: ["latin"] });

// METADATA
export const metadata: Metadata = {
  title: "SASOM Dashboard",
  description: "SASOM Dashboard By Pakawat Buranakunaporn",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={inter.className}>{children}</body>
    </html>
  );
}
