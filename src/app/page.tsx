// NEXT
import Image from "next/image";

// UI
import { Card } from "@/components/ui/card";

export default function Home() {
  return (
    <main className="min-h-dvh grid grid-cols-3 items-center justify-between p-12 gap-4">
      <div className="col-span-1 flex flex-col gap-4">
        <Card className="h-full"></Card>
      </div>
      <div className="col-span-2 h-full">main</div>
    </main>
  );
}
