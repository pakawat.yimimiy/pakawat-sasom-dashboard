import React from "react";

// COMPONENTS
import AddListingForm from "@/components/AddListing/AddListingForm";

const AddListing = () => {
  return (
    <div className="flex flex-col gap-4">
      {/* TITLE */}
      <h1 className="text-3xl">AddListing</h1>

      {/*  */}
      <AddListingForm />
    </div>
  );
};

export default AddListing;
